package ru.vlasova.iteco.taskmanager.api.service;

import ru.vlasova.iteco.taskmanager.entity.AbstractEntity;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    @Nullable
    List<E> findAll();

    @Nullable
    List<E> findAll(@Nullable String userId);

    @Nullable
    E findOne(@Nullable String id);

    @Nullable
    E findOneByUserId(@Nullable String userId, @Nullable String id);

    @Nullable
    E persist(@Nullable E obj) throws DuplicateException;

    void merge(@Nullable E obj);

    void remove(@Nullable String id);

    void removeAll();

    void removeAll(@Nullable String userId);

    default boolean isValid(String... strings){
        for (String str : strings)
            if (str == null || str.isEmpty())
                return false;
        return true;
    }

}
