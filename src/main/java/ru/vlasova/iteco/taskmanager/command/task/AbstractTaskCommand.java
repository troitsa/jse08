package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    @Nullable
    public Role[] getRole() {
        return new Role[]{Role.USER};
    }

    void printTaskList(@Nullable List<Task> taskList) {
        @NotNull int i = 1;
        if (taskList == null) return;
        for (@Nullable Task task : taskList) {
            serviceLocator.getTerminalService().print(i + ": " + task);
            i++;
        }
    }

    @Nullable List<Project> printProjectList(@Nullable String userId) {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final List<Project> projectList = projectService.findAll(userId);
        if (projectList == null || projectList.size() == 0) {
            terminalService.print("There are no projects.");
            return null;
        }
        int i = 1;
        for (@NotNull Project project : projectList) {
            terminalService.print(i++ + ": " + project);
        }
        return projectList;
    }

}
