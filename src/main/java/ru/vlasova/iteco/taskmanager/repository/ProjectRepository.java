package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.entity.Project;

import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    @Nullable
    public String getIdByIndex(@NotNull String userId, int projectIndex) {
        @Nullable final List<Project> projectList = findAll(userId);
        @Nullable final Project project = projectList.get(projectIndex);
        if (project == null) return null;
        @NotNull final String projectId = project.getId();
        return projectId;
    }

}
