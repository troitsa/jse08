package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.IOException;

public final class UserLogInCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    @NotNull
    public String getName() {
        return "user_login";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "User authorization";
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        terminalService.print("Login: ");
        @Nullable final String login = terminalService.readString();
        terminalService.print("Password: ");
        @Nullable final String password = terminalService.readString();
        @Nullable final User user = userService.login(login, password);
        if(user != null) {
            serviceLocator.getStateService().setCurrentUser(user);
            terminalService.print("Log in success");
        }
        else {
            terminalService.print("Login or password invalid");
        }
    }

}