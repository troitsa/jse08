package ru.vlasova.iteco.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.entity.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    @Nullable
    public Task getTaskByIndex(@NotNull final String userId, final int index) {
        return findAll(userId).get(index);
    }

    @Override
    public void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId) {
        @Nullable final List<Task> taskList = getTasksByProjectId(userId, projectId);
        for (@Nullable Task task : taskList) {
            remove(task.getId());
        }
    }

    @Override
    @NotNull
    public List<Task> getTasksByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final List<Task> taskList = new ArrayList<>();
        @Nullable final List<Task> tasks = findAll(userId);
        for (@NotNull Task task : tasks) {
            if (task.getProjectId().equals(projectId)) {
                taskList.add(task);
            }
        }
        return taskList;
    }

}
