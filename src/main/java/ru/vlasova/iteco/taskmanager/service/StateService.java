package ru.vlasova.iteco.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.repository.StateRepository;

import java.util.List;

public final class StateService {

    @NotNull private final StateRepository repository = new StateRepository();

    @NotNull
    public List<AbstractCommand> getCommands() {
        return repository.getCommands();
    }

    public void add(@NotNull final AbstractCommand abstractCommand) {
        repository.add(abstractCommand);
    }

    @Nullable
    public AbstractCommand get(@Nullable final String command) {
        if (command == null || command.isEmpty()) return null;
        return repository.get(command);
    }

    @Nullable
    public String getCurrentUserId() {
        return repository.getCurrentUser().getId();
    }

    @Nullable
    public User getCurrentUser() {
        return repository.getCurrentUser();
    }

    public void setCurrentUser(@Nullable final User user) {
        repository.setCurrentUser(user);
    }

}
