package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.IOException;
import java.util.List;

public final class TaskAttachCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    @NotNull
    public String getName() {
        return "task_attach";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Attach task to project";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        if (userId == null) {
            terminalService.print("You are not authorized");
            return;
        }
        terminalService.print("Choose the task and type the number");
        @Nullable final List<Task> taskList = taskService.findAll(userId);
        if (taskList == null) return;
        if (taskList.size() !=0) {
            printTaskList(taskList);
            final int index = Integer.parseInt(terminalService.readString()) - 1;
            @Nullable final Task task = taskService.getTaskByIndex(userId, index);
            if(task == null) return;
            terminalService.print("Attach task: " + task.getName() + ". Choose project id: ");
            printProjectList(userId);
            final int projectIndex = Integer.parseInt(terminalService.readString())-1;
            @Nullable final String projectId = serviceLocator.getProjectService().getProjectByIndex(userId, projectIndex).getId();
            task.setProjectId(projectId);
            terminalService.print("Task attached.");
        }
    }

}