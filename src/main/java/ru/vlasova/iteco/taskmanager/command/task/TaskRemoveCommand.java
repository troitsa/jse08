package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.IOException;
import java.util.List;

public final class TaskRemoveCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    @NotNull
    public String getName() {
        return "task_remove";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove selected task";
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        terminalService.print("Choose the task and type the number");
        if(userId == null) return;
        @Nullable final List<Task> taskList = taskService.findAll(userId);
        if(taskList == null || taskList.size() == 0) return;
        printTaskList(taskList);
        final int index = Integer.parseInt(terminalService.readString()) - 1;
        @Nullable final Task task = taskService.getTaskByIndex(userId, index);
        if (task != null) {
            taskService.remove(userId, task.getId());
            terminalService.print("Task deleted.");
        }
    }

}