package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.IOException;

public final class ProjectEditCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    @NotNull
    public String getName() {
        return "project_edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit selected project";
    }

    @Override
    public void execute() {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String userId = serviceLocator.getCurrentUser().getId();
        terminalService.print("Choose the task and type the number");
        printProjectList(userId);
        @Nullable final int index = Integer.parseInt(terminalService.readString())-1;
        @Nullable final Project project = projectService.getProjectByIndex(userId, index);
        if(project == null) return;
        terminalService.print("Editing task: " + project.getName() + ". Set new name: ");
        project.setName(terminalService.readString());
        terminalService.print("Task edit.");
    }

}