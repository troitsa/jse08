package ru.vlasova.iteco.taskmanager.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.List;


@Setter
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Getter
    private IProjectRepository repository;

    @NotNull
    private ITaskService taskService;

    public ProjectService(@NotNull final IProjectRepository repository, @NotNull final ITaskService taskService) {
        this.repository = repository;
        this.taskService = taskService;
    }

    @Override
    @Nullable
    public Project insert(@Nullable final String userId, @Nullable final String name,
                          @Nullable final String description, @Nullable final String dateStart,
                          @Nullable final String dateFinish) {
        final boolean checkGeneral = isValid(name, description, dateStart, dateFinish);
        if (!checkGeneral || userId == null) return null;
        @NotNull final Project project = new Project(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(DateUtil.parseDateFromString(dateStart));
        project.setDateFinish(DateUtil.parseDateFromString(dateFinish));
        return project;
    }

    @Override
    public void remove(@Nullable final String userId, final int index) {
        if (userId == null) return;
        @Nullable final Project project = getProjectByIndex(userId, index);
        if (project == null) return;
        repository.remove(userId, project.getId());
        taskService.removeTasksByProjectId(userId, project.getId());
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) {
        @Nullable final Project project = findOneByUserId(userId, id);
        if(project == null || userId == null) return;
        repository.remove(userId, project.getId());
        taskService.removeTasksByProjectId(userId, project.getId());
    }

    @Override
    @Nullable
    public Project getProjectByIndex(@Nullable final String userId, final int index) {
        if (userId == null || index < 0) return null;
        if(index > repository.count() || index < 0) return null;
        @Nullable String projectId = repository.getIdByIndex(userId, index);
        if (projectId == null) return null;
        return repository.findOne(projectId);
    }

    @Override
    @Nullable
    public List<Task> getTasksByProjectIndex(@Nullable final String userId, final int projectIndex) {
        if (userId == null || projectIndex < 0 ) return null;
        @Nullable String projectId = repository.getIdByIndex(userId, projectIndex);
        if(projectId == null) return null;
        return taskService.getTasksByProjectId(userId, projectId);
    }

}
