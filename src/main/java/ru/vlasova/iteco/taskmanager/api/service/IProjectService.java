package ru.vlasova.iteco.taskmanager.api.service;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    Project insert(@Nullable String userId, @Nullable String name, @Nullable String description, @Nullable String dateStart, @Nullable String dateFinish);

    @Nullable
    Project getProjectByIndex(@Nullable String userId, int index);

    void remove(@Nullable String userId, @Nullable String id);

    void remove(@Nullable String userId, int index);

    @Nullable
    List<Task> getTasksByProjectIndex(@Nullable String userId, int projectIndex);

}
