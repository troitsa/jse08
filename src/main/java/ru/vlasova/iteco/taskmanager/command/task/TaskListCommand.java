package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    @NotNull
    public String getName() {
        return "task_list";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        if (userId == null) return;
        @Nullable final List<Task> taskList = serviceLocator.getTaskService().findAll(userId);
        if(taskList == null || taskList.size() == 0) {
            terminalService.print("There are no tasks.");
        }
        else {
            printTaskList(taskList);
        }
    }

}