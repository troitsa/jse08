package ru.vlasova.iteco.taskmanager.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Getter
    private final IUserRepository repository;

    public UserService(@NotNull IUserRepository repository) {
        this.repository = repository;
    }

    @Override
    public void edit(@Nullable final User user, @Nullable final String login, @Nullable final String password) {
        if (!isValid(login, password)) return;
        user.setLogin(login);
        user.setPwd(password);
    }

    @Override
    @Nullable
    public User insert(@Nullable final String login, @Nullable final String password) {
        final boolean checkGeneral = isValid(login, password);
        if (!checkGeneral) return null;
        return new User(login, password);
    }

    @Override
    @Nullable
    public User login(final String login, final String password) {
        @Nullable final String id = checkUser(login);
        if(id == null) return null;
        @Nullable final User user = repository.findOne(id);
        if (user == null) return null;
        @Nullable final String pwd = user.getPwd();
        if (pwd == null) return null;
        if(pwd.equals(HashUtil.MD5(password))) {
            return user;
        }
        else {
            return null;
        }
    }

    @Override
    @Nullable
    public String checkUser(final String login) {
        if(!isValid(login)) return null;
        @Nullable String id = repository.checkUser(login);
        if (id == null) return null;
        return id;
    }

    @Override
    public boolean checkRole(@Nullable final String userId, @Nullable final Role[] roles) {
        if(userId == null || roles == null) return false;
        @Nullable User user = findOne(userId);
        if(user == null) return false;
        for (@Nullable Role role : roles) {
            if(user.getRole().equals(role)) return true;
        }
        return false;
    }



}
