package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.IOException;
import java.util.List;

public final class TaskListByProjectCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    @NotNull
    public String getName() {
        return "task_list_by_project";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show tasks by selected project";
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        @NotNull final IProjectService service = serviceLocator.getProjectService();
        if(userId == null) return;
        printProjectList(userId);
        terminalService.print("Please, choose the project and type the number");
        @Nullable final int index = Integer.parseInt(terminalService.readString())-1;
        @Nullable final List<Task> taskList = service.getTasksByProjectIndex(userId, index);
        if(taskList == null || taskList.size()==0) {
            terminalService.print("There are no tasks.");
        }
        else {
            printTaskList(taskList);
        }
    }

}