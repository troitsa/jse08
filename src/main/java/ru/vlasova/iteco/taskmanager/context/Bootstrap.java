package ru.vlasova.iteco.taskmanager.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.repository.IProjectRepository;
import ru.vlasova.iteco.taskmanager.api.repository.ITaskRepository;
import ru.vlasova.iteco.taskmanager.api.repository.IUserRepository;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.repository.ProjectRepository;
import ru.vlasova.iteco.taskmanager.repository.TaskRepository;
import ru.vlasova.iteco.taskmanager.repository.UserRepository;
import ru.vlasova.iteco.taskmanager.service.*;
import ru.vlasova.iteco.taskmanager.api.service.ServiceLocator;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.CommandCorruptException;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator  {

    @Getter
    @NotNull
    private final StateService stateService = new StateService();

    @Getter
    @NotNull
    private final TerminalService terminalService = new TerminalService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, taskService);

    private void registry(@NotNull final AbstractCommand command) {
        @NotNull final String cliCommand = command.getName();
        @NotNull final String cliDescription = command.getDescription();

        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();

        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();

        command.setServiceLocator(this);
        stateService.add(command);
    }

    public void start() {
        createUsers();
        terminalService.print("*** WELCOME TO TASK MANAGER ***");
        @NotNull String command = "";
        while (true) {
            command = terminalService.readString();
            execute(command);
        }
    }

    private void execute(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = stateService.get(command);
        if (abstractCommand == null) return;
        @NotNull final boolean checkAccess = !abstractCommand.secure() || abstractCommand.secure() &&
                stateService.getCurrentUserId() != null;
        @Nullable final Role[] roles = abstractCommand.getRole();
        @NotNull final boolean checkRole = abstractCommand.getRole() == null ||
                userService.checkRole(stateService.getCurrentUserId(), roles);
        try {
            if (checkAccess && checkRole) {
                abstractCommand.execute();
            } else {
                terminalService.print("Access denied. Please, login");
            }
        } catch (DuplicateException | IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @NotNull
    public Bootstrap init(@NotNull final Class[] classes) {
        for (@NotNull Class clazz : classes) {
            try {
                if (AbstractCommand.class.isAssignableFrom(clazz)) {
                    registry((AbstractCommand) clazz.newInstance());
                }
            } catch (InstantiationException | IllegalAccessException e) {
                System.out.println(e.getMessage());
            }
        }
        return this;
    }

    private void createUsers() {
        @NotNull final User user = new User(Role.USER);
        user.setLogin("user");
        user.setPwd("qwerty");
        @NotNull final User admin = new User(Role.ADMIN);
        admin.setLogin("admin");
        admin.setPwd("1234");
        try {
            getUserService().persist(user);
            getUserService().persist(admin);
        } catch (DuplicateException e) {
            System.out.println(e.getMessage());
        }
        stateService.setCurrentUser(user);
    }

}