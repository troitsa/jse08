package ru.vlasova.iteco.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.service.StateService;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

public interface ServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    TerminalService getTerminalService();

    @NotNull
    StateService getStateService();

    @Nullable
    default User getCurrentUser() {
        return getStateService().getCurrentUser();
    }

    @Nullable
    default String getCurrentUserId() {
        return getStateService().getCurrentUserId();
    }

}