package ru.vlasova.iteco.taskmanager.repository;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;

import java.util.*;

public final class StateRepository {

    @Getter
    @Setter
    @Nullable
    private User currentUser = null;

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void add(@NotNull final AbstractCommand abstractCommand) {
        commands.put(abstractCommand.getName(), abstractCommand);
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Nullable
    public AbstractCommand get(@NotNull final String command) {
        return commands.get(command);
    }

}
