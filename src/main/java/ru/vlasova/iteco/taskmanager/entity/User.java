package ru.vlasova.iteco.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.util.HashUtil;

@NoArgsConstructor
@Getter
@Setter
public final class User extends AbstractEntity {

    @Nullable
    private String login = "";

    @Nullable
    private String pwd = "";

    @NotNull
    private Role role = Role.USER;

    public User(@NotNull final Role role) {
        this.role = role;
    }

    public User(@NotNull final String login, @NotNull final String pwd) {
        this.login = login;
        this.pwd = HashUtil.MD5(pwd);
    }

    @Override
    @NotNull
    public String toString() {
        return "User: " + login + ", " + role;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @Nullable final User user = (User) o;
        return login.equals(user.getLogin());
    }

}
