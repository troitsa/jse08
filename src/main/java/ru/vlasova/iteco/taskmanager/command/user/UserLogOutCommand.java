package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

public final class UserLogOutCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    @NotNull
    public Role[] getRole() {
        return new Role[] {Role.USER};
    }

    @Override
    @NotNull
    public String getName() {
        return "user_logout";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Logout user";
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        serviceLocator.getStateService().setCurrentUser(null);
        terminalService.print("Log out success");
    }

}