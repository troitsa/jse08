package ru.vlasova.iteco.taskmanager.api.service;

import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

public interface IUserService extends IService<User> {

    User insert(String login, String password);

    User login(String login, String password);

    String checkUser(String login);

    void edit(User user, String login, String password);

    boolean checkRole(String userId, Role[] roles);

}
