package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.IOException;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    @NotNull
    public String getName() {
        return "task_create";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new task";
    }

    @Override
    public void execute() throws DuplicateException {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        terminalService.print("Creating task. Set name: ");
        @Nullable final String name = terminalService.readString();
        terminalService.print("Input description: ");
        @Nullable final String description = terminalService.readString();
        terminalService.print("Set start date: ");
        @Nullable final String dateStart = terminalService.readString();
        terminalService.print("Set end date: ");
        @Nullable final String dateFinish = terminalService.readString();
        @NotNull final Task task = taskService.insert(userId, name, description, dateStart, dateFinish);
        if(task == null) return;
        taskService.persist(task);
        terminalService.print("Task created.");
    }

}