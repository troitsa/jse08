package ru.vlasova.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.ITaskService;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.IOException;
import java.util.List;

public final class TaskEditCommand  extends AbstractTaskCommand {

    @Override
    @Nullable
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    @NotNull
    public String getName() {
        return "task_edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit selected task";
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        if (userId == null) {
            terminalService.print("You are not authorized");
            return;
        }
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("Choose the task and type the number");
        @Nullable final List<Task> taskList = taskService.findAll(userId);
        if (taskList != null && taskList.size() !=0) {
            printTaskList(taskList);
            final int index = Integer.parseInt(terminalService.readString()) - 1;
            @Nullable final Task task = taskService.getTaskByIndex(userId, index);
            if (task == null) return;
            terminalService.print("Editing task: " + task.getName() + ". Set new name: ");
            @Nullable final String name = terminalService.readString();
            if (name == null) return;
            task.setName(name);
            terminalService.print("Set new description: ");
            @Nullable final String description = terminalService.readString();
            if (description == null) return;
            task.setDescription(description);
            terminalService.print("Choose project id: ");
            printProjectList(userId);
            final int projectIndex = Integer.parseInt(terminalService.readString())-1;
            @Nullable final String projectId = serviceLocator.getProjectService()
                    .getProjectByIndex(userId, projectIndex).getId();
            if (projectId == null) return;
            task.setProjectId(projectId);
            terminalService.print("Task edit.");
        }
    }

}