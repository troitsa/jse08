package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    String checkUser(@NotNull final String login);

}
