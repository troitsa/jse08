package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.error.DuplicateException;
import ru.vlasova.iteco.taskmanager.service.TerminalService;


import java.io.IOException;

public final class UserRegistrationCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    @NotNull
    public Role[] getRole() {
        return new Role[] {Role.USER};
    }

    @Override
    @NotNull
    public String getName() {
        return "user_registration";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "To register new user";
    }

    @Override
    public void execute() throws DuplicateException {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        terminalService.print("Creating user. Set login: ");
        @Nullable final String login = terminalService.readString();
        terminalService.print("Set password: ");
        @Nullable final String password = terminalService.readString();
        if(login == null || password == null) return;
        @Nullable final User user = userService.insert(login, password);
        if(user == null) {
            terminalService.print("User is not registered. Try again");
        } else {
            userService.persist(user);
            terminalService.print("User is registered.");
        }
    }

}