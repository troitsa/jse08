package ru.vlasova.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;
import ru.vlasova.iteco.taskmanager.command.project.*;
import ru.vlasova.iteco.taskmanager.command.system.*;
import ru.vlasova.iteco.taskmanager.command.task.*;
import ru.vlasova.iteco.taskmanager.command.user.*;
import ru.vlasova.iteco.taskmanager.context.Bootstrap;

public final class App {

    @NotNull
    private final static Class[] CLASSES = new Class[] { ProjectClearCommand.class, ProjectCreateCommand.class,
            ProjectEditCommand.class, ProjectListCommand.class, ProjectRemoveCommand.class,
            TaskAttachCommand.class, TaskClearCommand.class, TaskCreateCommand.class, TaskEditCommand.class,
            TaskListByProjectCommand.class, TaskListCommand.class, TaskRemoveCommand.class,
            TaskDetachCommand.class, UserChangePassCommand.class, UserEditCommand.class, UserLogInCommand.class,
            UserLogOutCommand.class, UserRegistrationCommand.class, UserViewCommand.class, UserRemoveCommand.class,
            HelpCommand.class, ExitCommand.class, AboutCommand.class };

    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES).start();
    }

}