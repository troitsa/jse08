package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    @NotNull
    public String getName() {
        return "project_list";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        if(userId == null) return;
        printProjectList(userId);
    }

}