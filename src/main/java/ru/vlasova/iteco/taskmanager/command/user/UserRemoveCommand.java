package ru.vlasova.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IUserService;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.User;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.IOException;
import java.util.List;

public final class UserRemoveCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    @NotNull
    public String getName() {
        return "user_remove";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove user";
    }

    @NotNull
    public Role[] getRole() {
        return new Role[] {Role.ADMIN};
    }

    @Override
    public void execute() {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final User user = serviceLocator.getCurrentUser();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        if (user == null) {
            terminalService.print("You are not authorized");
            return;
        }
        @Nullable final List<User> userList = userService.findAll();
        terminalService.print("Choose user:");
        for(@NotNull User u : userList) {
            terminalService.print(u.getLogin());
        }
        @Nullable final String userLogin = terminalService.readString();
        userService.remove(userService.checkUser(userLogin));
        terminalService.print("User removed");
    }

}
