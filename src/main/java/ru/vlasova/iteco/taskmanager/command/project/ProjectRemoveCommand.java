package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.api.service.IProjectService;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.IOException;

public final class ProjectRemoveCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    @NotNull
    public String getName() {
        return "project_remove";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove selected project";
    }

    @Override
    public void execute() {
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String userId = serviceLocator.getCurrentUserId();
        if(userId == null) return;
        printProjectList(userId);
        terminalService.print("To delete project choose the project and type the number");
        @Nullable final String indexString = terminalService.readString();
        if(indexString == null || indexString.trim().length() == 0) {
            terminalService.print("No such project.");
            return;
        }
        final int index = Integer.parseInt(indexString)-1;
        if(index < 0) return;
        projectService.remove(userId, index);
        terminalService.print("Project delete.");
    }

}