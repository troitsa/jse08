package ru.vlasova.iteco.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Nullable
    Task getTaskByIndex(@NotNull final String userId, final int index);

    void removeTasksByProjectId(@NotNull final String userId, @NotNull final String projectId);

    @Nullable
    List<Task> getTasksByProjectId(@NotNull final String userId, @NotNull final String projectId);

}
