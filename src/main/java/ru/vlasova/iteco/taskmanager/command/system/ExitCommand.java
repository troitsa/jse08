package ru.vlasova.iteco.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.enumeration.Role;
import ru.vlasova.iteco.taskmanager.service.TerminalService;

import java.io.IOException;

public class ExitCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    @Nullable
    public Role[] getRole() {
        return super.getRole();
    }

    @Override
    @NotNull
    public String getName() {
        return "exit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Exit from application.";
    }

    @Override
    public void execute() throws IOException {
        @NotNull final TerminalService terminalService = serviceLocator.getTerminalService();
        terminalService.getReader().close();
        System.exit(1);
    }

}