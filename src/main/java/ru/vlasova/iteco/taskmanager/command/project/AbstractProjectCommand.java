package ru.vlasova.iteco.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vlasova.iteco.taskmanager.command.AbstractCommand;
import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.enumeration.Role;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    public void printProjectList(@Nullable final String userId) {
        @Nullable List<Project> projectList = serviceLocator.getProjectService().findAll(userId);
        if(projectList != null && projectList.size()!=0) {
            @NotNull int i = 1;
            for (Project project : projectList) {
                serviceLocator.getTerminalService().print(i++ + ": " + project);
            }
        }
        else {
            serviceLocator.getTerminalService().print("There are no projects.");
        }
    }

    @Override
    public Role[] getRole() {
        return new Role[] {Role.USER};
    }

}
